from selenium import webdriver
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

# @pytest.mark.usefixtures("setup")
from pegeObjects.CheckoutPage import Checkout
from pegeObjects.HomePage import HomePage
from pegeObjects.OrderPage import Order
from pegeObjects.ShopPage import Shop
from testData.SearchData import SearchPageData
from utilities.BaseClass import BaseClass


class TestOne(BaseClass):

    def test_e2e(self, setup, getData):
        log = self.getLogger()
        hp = HomePage(self.driver)
        # sp = Shop(self.driver)
        # ck = Checkout(self.driver)
        # ord = Order(self.driver)

        log.info("Browser & URL invoked")

        # hp.shopItem().click()
        sp = hp.shopItem()
        log.info("Shop button clicked")

        products = sp.produtsItem()

        # //div[@class='card h-100']/div/h4/a
        # product =//div[@class='card h-100']
        for product in products:
            productName = sp.prod_list().text
            if productName == getData["prod"]:
                # Add item into cart
                sp.prod_btn.click()
        log.info("Product selected")
        ck = sp.Checkout_Btn()
        log.info("Checkout button clicked")

        ord = ck.Checkout_Final()
        log.info("Final checkout")

        # ord.Country().send_keys(getData[1])
        ord.Country().send_keys(getData["ctr"])
        log.info("Country code enter")
        self.verifyLinkPresence(getData["country"])
        ord.Country_Btn().click()
        log.info("Country Selected")
        ord.Agree_Check().click()
        ord.Submit().click()
        log.info("Agree & submit button clicked")
        successText = ord.Alert_Msg().text
        # log.info("Text received from application", successText)

        assert "aaaaaSuccess! Thank you!" in successText

    # @pytest.fixture(params=[("Blackberry", "ind", "India"), ("Samsung Note 8", "aus", "Austria")]
    # @pytest.fixture(params=[("Blackberry", "ind", "India")])
    # @pytest.fixture(params=SearchPageData.test_data)
    # def getData(self, request):
        # return request.param

    @pytest.fixture(params=SearchPageData.getTestData("TC_1"))
    def getData(self, request):
        return request.param

