from selenium.webdriver.common.by import By

from pegeObjects.CheckoutPage import Checkout


class Shop:

    def __init__(self, driver):
        self.driver = driver

    products = (By.XPATH, "//div[@class='card h-100']")

    def produtsItem(self):
        return self.driver.find_elements(*Shop.products)

    prod_item = (By.XPATH, "//div[@class='card h-100']/div/h4/a")

    def prod_list(self):
        return self.driver.find_element(*Shop.prod_item)

    prod_btn = (By.XPATH, "//div[@class='card h-100']/div/button")

    def product_button(self):
        return self.driver.find_element(*Shop.prod_btn)

    chk_btn = (By.CSS_SELECTOR, "a[class*='btn-primary']")

    def Checkout_Btn(self):
        self.driver.find_element(*Shop.chk_btn).click()
        ck = Checkout(self.driver)
        return ck
