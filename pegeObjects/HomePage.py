from selenium.webdriver.common.by import By

from pegeObjects.ShopPage import Shop


class HomePage:

    def __init__(self, driver):
        self.driver = driver

    shop = (By.CSS_SELECTOR, "a[href*='shop']")

    def shopItem(self):
        self.driver.find_element(*HomePage.shop).click()
        sp = Shop(self.driver)
        return sp
