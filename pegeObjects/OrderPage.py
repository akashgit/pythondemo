from selenium.webdriver.common.by import By


class Order:

    def __init__(self, driver):
        self.driver = driver

    ctr = (By.ID, "country")

    def Country(self):
        return self.driver.find_element(*Order.ctr)

    ctr_btn = (By.LINK_TEXT, "India")

    def Country_Btn(self):
        return self.driver.find_element(*Order.ctr_btn)

    agree = (By.XPATH, "//div[@class='checkbox checkbox-primary']")

    def Agree_Check(self):
        return self.driver.find_element(*Order.agree)

    submt = (By.CSS_SELECTOR, "[type='submit']")

    def Submit(self):
        return self.driver.find_element(*Order.submt)

    alrt = (By.CLASS_NAME, "alert-success")

    def Alert_Msg(self):
        return self.driver.find_element(*Order.alrt)
