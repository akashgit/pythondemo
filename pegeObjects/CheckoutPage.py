from selenium.webdriver.common.by import By

from pegeObjects.OrderPage import Order


class Checkout:

    def __init__(self, driver):
        self.driver = driver

    # self.driver.find_element_by_xpath("//button[@class='btn btn-success']")
    chkt = (By.XPATH, "//button[@class='btn btn-success']")

    def Checkout_Final(self):
        self.driver.find_element(*Checkout.chkt).click()
        ord = Order(self.driver)
        return ord